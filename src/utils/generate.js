const initBombs = () => [...new Array(10).fill({ bomb: true }), ...new Array(71).fill({ bomb: false })]
.sort(() => Math.random() - 0.5)

export default (count = 9) => {
    const data = initBombs()
    .map((item, id) => ({
        ...item,
        id,
        count: 0,
        visible: false,
        marked: false,
        c: id % count,
        r: ~~(id / count),
        p: [],
    })).reduce((acc, item) => ({
        ...acc,
        [item.id]: item,
    }), {})


    Object.keys(data).map(id_ => {
        const id = +id_
        const el = data[id]
        const p = el.p

        if (data.hasOwnProperty(id - count)) p.push(+id - count)
        if (data.hasOwnProperty(id + count)) p.push(+id + count)

        if (el.c > 0) {
            if (data.hasOwnProperty(id - 1)) p.push(id - 1)
            if (data.hasOwnProperty(id - count - 1)) p.push(id - count - 1)
            if (data.hasOwnProperty(id + count - 1)) p.push(id + count - 1)
        }
        if (el.c < count - 1) {
            if (data.hasOwnProperty(id + 1)) p.push(id + 1)
            if (data.hasOwnProperty(id - count + 1)) p.push(id - count + 1)
            if (data.hasOwnProperty(id + count + 1)) p.push(id + count + 1)
        }

        if (data[id].bomb) {
            p.forEach(id => {
                data[id].count += 1
            })
        }
    })

    return data
}
