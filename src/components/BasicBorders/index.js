import styled from 'styled-components'

const OuterBorder = styled.div`
    border-width: 0.3em;
    border-style: solid;
    border-color: #FFFFFF #7f7f7f #7f7f7f #FFFFFF;
`
const InnerBorder = styled.div`
      border-width: 0.3em;
      border-style: solid;
      border-color: #7f7f7f #FFFFFF #FFFFFF #7f7f7f;
`

export default {
    OuterBorder,
    InnerBorder,
}