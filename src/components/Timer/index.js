import React, { Component } from 'react'
import styled from 'styled-components'

const StyledTimer = styled.div`
    width: 100px;
    height: 36px;
    background: black;
    padding: 0 10px;
    font-size: 30px;
    text-align: right;
    vertical-align: middle;
    font-weight: bold;
    color: red;
    font-family: sans-serif;
    letter-spacing: 0.2em;
    
    @media (max-width: 440px) {
        width: 16vw;
        height: 8vw;
        font-size: 6vw;
        text-align: right;
    }
`

export default class extends Component {
    timer = null

    state = {
        seconds: 0,
    }
    start = () => {
        this.clear()
        this.timer = setInterval(() => {
            this.tick()
        }, 1000)
    }
    stop = () => {
        this.props.setWinTime(this.state.seconds)
        clearInterval(this.timer)
    }
    clear = () => {
        this.setState({
            seconds: 0,
        })
    }

    componentDidUpdate(prevProps) {
        if (prevProps.started && !this.props.started) {
            this.stop()
        }
        if (!prevProps.started && this.props.started) {
            this.start()
        }
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    tick() {
        this.setState({
            seconds: this.state.seconds + 1,
        })
    }

    render() {
        return (
            <StyledTimer>{this.state.seconds.toString().padStart(3, '0')}</StyledTimer>
        )
    }
}