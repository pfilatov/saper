import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import Borders from '../BasicBorders'


const Modal = styled(Borders.OuterBorder)`
    width: 70%;
    height: 80px;
    background: silver;
    display: flex;
    align-items: center;
    justify-content: space-around;
    flex-direction: column;
`

const ModalTitle = styled.h3`
    margin: 5px 0 0;
`

const ModalButton = styled.button`
    padding: 4px 10px;
    border-width: 0.2em;
    border-style: solid;
    border-color: #FFFFFF #7f7f7f #7f7f7f #FFFFFF;
    background: silver;
`

const ModalOverLay = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0.5);
    display: flex;
    justify-content: center;
    align-items: center;
`

export default ({ win, time }) => {
    const [visible, show] = useState(false)
    useEffect(() => {
        win && show(true)
    }, [win])
    return visible ? (
        <ModalOverLay>
            <Modal>
                <ModalTitle>You win! Time: {time}s</ModalTitle>
                <ModalButton onClick={() => show(false)}>Ok!</ModalButton>
            </Modal>
        </ModalOverLay>
    ) : null
}