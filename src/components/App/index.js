import React, { Component } from 'react'
import styled from 'styled-components'
import Item from '../Item'
import generate from '../../utils/generate'
import ControlBlock from '../ControlBlock'
import Modal from '../Modal'
import Borders from '../BasicBorders'

const Title = styled.h1`
    width: 100%;
    height: 40px;
    line-height: 30px;
    font-size: 30px;
    margin-top: 0;
    background: black;
    text-align: center;
    color: white;
`

const Grid = styled.section`
    display: flex;
    justify-content: center;
`

const Area = styled.div`
    display: grid;
    width: 360px;
    grid-template-columns: repeat(9, 40px);
    
    @media (max-width: 440px) {
        width: 81vw;
        grid-template-columns: repeat(9, 9vw);
    }
`

const FieldWrapper = styled(Borders.InnerBorder)`
    width: 360px;
    height: 360px;
    
    @media (max-width: 440px) {
        width: 81vw;
        height: 81vw;
    }
`

const Container = styled(Borders.OuterBorder)`
    position: relative;
    width: 400px;
    height: 470px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
    background: silver;
    
    @media (max-width: 440px) {
        width: 90vw;
        height: 110vw;
    }
`

export default class extends Component {
    state = {
        data: {},
        started: false,
        over: false,
        count: 0,
        mode: 'flip',
        winTime: 0,
    }

    start = () => {
        this.setState({
            data: generate(),
            over: false,
            started: false,
            win: false,
            count: 0,
            mode: 'flip',
        })
    }

    flip = (props) => () => {
        const { data, count } = this.state
        const ref = { count }
        if (props.bomb) {
            data[props.id].visible = true
            this.setState({
                over: true,
                started: false,
                data,
            })
        } else {
            this.flipRecurse(data, props.id, ref)

            this.setState({
                data,
                started: ref.count !== 71,
                count: ref.count,
                win: ref.count === 71,
            })
        }
    }

    flipRecurse = (obj, id, ref) => {
        if (!obj[id].visible && !obj[id].marked) {
            obj[id].visible = true
            ref.count += 1
            if (!obj[id].bomb && !obj[id].count) {
                obj[id].p.forEach(item => {
                    this.flipRecurse(obj, item, ref)
                })
            }
        }
    }

    mark = (props) => () => {
        const { data } = this.state

        data[props.id].marked = !data[props.id].marked

        this.setState({
            data,
            started: true,
        })
    }

    toggleMode = () => {
        this.setState({
            mode: this.state.mode === 'flip' ? 'mark' : 'flip',
        })
    }

    setWinTime = winTime => {
        this.setState({
            winTime,
        })
    }

    componentDidMount() {
        this.start()
    }

    render() {
        const { win, over, mode, started, winTime } = this.state

        return (
            <div>
                <Title>Saper</Title>
                <Grid>
                    <Container>
                        <Modal win={win} time={winTime}/>
                        <ControlBlock
                            win={win}
                            over={over}
                            mode={mode}
                            started={started}
                            start={this.start}
                            toggleMode={this.toggleMode}
                            setWinTime={this.setWinTime}
                        />
                        <FieldWrapper>
                            <Area>
                                {Object.values(this.state.data).map(props => (
                                    <Item onFlip={mode === 'flip' ? this.flip(props) : this.mark(props)}
                                          mode={this.state.mode}
                                          key={props.id}
                                          over={over}
                                          win={win}
                                          {...props}/>
                                ))}
                            </Area>
                        </FieldWrapper>
                    </Container>
                </Grid>
            </div>
        )
    }
}