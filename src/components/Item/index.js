import React from 'react'
import styled, { css } from 'styled-components'
import bombSvg from '../../assets/bomb.svg'
import markSvg from '../../assets/mark.svg'

const StyledItem = styled.div`
  display: flex;
  height: 40px;
  box-sizing: border-box;
  background: ${({ bomb, visible }) => !visible ? 'silver' : bomb ? 'red' : 'silver' };
  background-image: ${({ bomb, over, marked, svg, markSvg }) =>
    bomb && over ? `url(${svg})` : marked ? `url(${markSvg})` : ''};
  background-size: 1.3em;
  background-repeat: no-repeat;
  background-position: center;
  word-break: break-all;
  justify-content: center;
  align-items: center;
  font-weight: bold;          
  ${({ visible }) => !visible ?
    css`
      border-width: 0.3em;
      border-style: solid;
      border-color: #FFFFFF #7f7f7f #7f7f7f #FFFFFF
  ` :
    css`
    box-shadow: inset 0.1em 0.1em 0 0.01em #7f7f7f
  `};
  
  @media (max-width: 440px) {
    height: calc(9vw);
  }
`


export default ({ onFlip, bomb, count, visible, over, marked }) => {
    return (
        <StyledItem
            onClick={!over && !visible ? onFlip : () => {
            }}
            bomb={bomb}
            visible={visible}
            svg={bombSvg}
            markSvg={markSvg}
            over={over}
            marked={marked}
        >
            {count && !bomb && visible ? count : null}
        </StyledItem>
    )
}