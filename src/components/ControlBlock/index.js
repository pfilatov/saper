import React, { Component } from 'react'
import styled from 'styled-components'
import bombSvg from '../../assets/bomb.svg'
import markSvg from '../../assets/mark.svg'
import Timer from '../Timer'
import Borders from '../BasicBorders'

const ControlBorder = styled(Borders.InnerBorder)`
  width:  360px;
  height: 50px;
  
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  
  @media (max-width: 440px) {
    width: 81vw;
    height: 12vw;
  }
`

const Smile = styled(Borders.OuterBorder)`
    width: 40px;
    height: 36px;
    border-width: 0.17em;
        
    font-size: 30px;
    text-align: center;
    
    @media (max-width: 440px) {
      width: 8vw;
      height: 8vw;
      font-size: 6vw;
      text-align: center;
  }
`

export const Mode = styled(Borders.OuterBorder)`
     width: 40px;
    height: 36px;
    border-width: 0.17em;
    
    background-image: url(${({ mode }) => mode === 'flip' ? bombSvg : markSvg });
    background-position: center;
    background-size: 1em;
    background-repeat: no-repeat;
    
    font-size: 30px;
    text-align: center;
    
    @media (max-width: 440px) {
      width: 8vw;
      height: 8vw;
      font-size: 6vw;
      text-align: center;
  }
`

export const Stub = styled.div`
  width: 100px;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
`

export default class ControlBlock extends Component {

    timer = null

    getSmile = () => {
        const { over, win } = this.props

        if (win) return '😎'
        if (over) return '😔'

        return '😊'
    }

    onStart = () => {
        this.props.start()
        this.timer.clear()
    }

    render() {
        return (
            <ControlBorder>
                <Stub><Mode mode={this.props.mode} onClick={this.props.toggleMode}/></Stub>
                <Smile onClick={this.onStart}>{this.getSmile()}</Smile>
                <Stub>
                    <Timer
                        ref={r => this.timer = r}
                        started={this.props.started}
                        setWinTime={this.props.setWinTime}
                    />
                </Stub>
            </ControlBorder>
        )
    }
}